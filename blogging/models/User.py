# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.db.models import Model, CharField, DateTimeField, ForeignKey, CASCADE


class UserModel(Model):
    firstname = CharField(max_length=20)
    lastname = CharField(max_length=50)
    date_create = DateTimeField(auto_now_add=True)
    user_auth = ForeignKey(User, on_delete=CASCADE)

    class Meta:
        db_table = 'User'
        app_label = 'blogging'
        ordering = ('-date_create',)
