# -*- coding: utf-8 -*-

from django import db
from django.utils.timezone import datetime

from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.generics import CreateAPIView, RetrieveAPIView
from rest_framework.response import Response

from blogging.models.posts import PostsModel
from blogging.serializers.post import PostSerializer, PostsFilterSerializer
from blogging.permissions.post_permissions import IsOwnerOrReadOnly

from blogging.models.User import UserModel
from blogging.serializers.user import UserSerializer


@api_view(['GET'])
def hello_world(request):
    return Response({
        'hand_shake': 'hello world',
        'message': 'It is working'
    })


class PostsViews(viewsets.ModelViewSet):
    queryset = PostsModel.objects.all()
    serializer_class = PostSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)

    def list(self, request, *args, **kwargs):
        serializer = PostsFilterSerializer()(data=request.query_params)
        if serializer.is_valid():
            filter_fields = {
                'date_start': datetime.fromtimestamp(serializer.data.get('date_start')),
                'date_end': datetime.fromtimestamp(serializer.data.get('date_end'))
            }
            order_type_raw = serializer.data.get('order_type')
            order_type = '-' if order_type_raw == 'DESC' else ''
            order_by = order_type + serializer.data.get('order_by')
            queryset = PostsModel.objects.filter(
                date_create__lte=filter_fields.get('date_end'),
                date_create__gte=filter_fields.get('date_start')
                ).order_by(order_by)
            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    def perform_destroy(self, instance):
        instance.deleted = True
        instance.save()

    def perform_create(self, serializer):
        user = UserModel.objects.get(user_auth=self.request.user)
        serializer.save(owner=user)


class UserViews(CreateAPIView):
    queryset = UserModel.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.AllowAny]

    def create(self, request, *args, **kwargs):
        try:
            return super(UserViews, self).create(request, *args, **kwargs)
        except db.IntegrityError:
            return Response({'message': 'user_exist'}, status=400)
        except db.OperationalError:
            return Response({'message': 'internal_error'}, status=500)
