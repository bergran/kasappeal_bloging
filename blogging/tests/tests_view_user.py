# -*- coding: utf-8 -*-

from json import loads

from rest_framework.test import APITestCase, APIClient
from rest_framework import status

from blogging.models.User import User, UserModel


class UserApiCreateTest(APITestCase):
    owner = {
        'username': 'cordelia',
        'password': 'Cordelia1',
        'firstname': 'Cordelia',
        'lastname': 'Burton',
        'email': 'cordelia@cordelia.com'
    }

    owner2 = {
        'username': 'peter',
        'password': 'Peter1',
        'firstname': 'Peter',
        'lastname': 'Pan',
        'email': 'peter@peter.com'
    }

    def setUp(self):
        # Creating user
        user = User.objects.create(
                                   username=self.owner['username'],
                                   email=self.owner['email'])
        user.set_password(self.owner['password'])

        # Creating owner to Post
        owner = UserModel.objects.create(
                                         user_auth=user,
                                         firstname=self.owner['firstname'],
                                         lastname=self.owner['lastname']
                                        )

    def test_create_post(self):
        client = APIClient()
        response = client.post('/api/v1/users', {
            'username': self.owner2['username'],
            'password': self.owner2['password'],
            'email': self.owner2['email'],
            'firstname': self.owner2['firstname'],
            'lastname': self.owner2['lastname']
        })

        # Check status code is correct
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response_data = loads(response.content)

        # Check return id
        self.assertTrue('id', response_data.keys())

        # Check return username
        self.assertTrue('username' in response_data.keys())

        # Check return firstname
        self.assertTrue('firstname' in response_data.keys())

        # Check return lastname
        self.assertTrue('lastname' in response_data.keys())

        # Check return email
        self.assertTrue('email' in response_data.keys())

    def test_bad_get_request(self):
        client = APIClient()
        response = client.get('/api/v1/users')

        # Check get method is not allowed
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_bad_edit_request(self):
        client = APIClient()
        response = client.put('/api/v1/users')

        # Check get method is not allowed
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
