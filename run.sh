#!/bin/bash

DOCKER_IMAGE=kasappeal_django_rest
DOCKER_NAME=kasappeal_container_django_rest
DOCKER_NGINX_NAME=kasappeal_container_nginx_rest
PORT=8000
NGINX_PORT=80
VOLUME=`pwd`

docker rm -f $DOCKER_NAME
docker rm -f $DOCKER_NGINX_NAME

docker run --name $DOCKER_NAME \
      -p $PORT:8000 \
      -v $VOLUME:/usr/src/app \
      -dt $DOCKER_IMAGE

docker run --name $DOCKER_NGINX_NAME \
      -p $NGINX_PORT:80 \
      -v ${VOLUME}/nginx.conf:/etc/nginx/nginx.conf:ro \
      -d nginx:1-alpine
