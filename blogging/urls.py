# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from blogging.views import PostsViews
from blogging.views import UserViews
from blogging.views import hello_world

router = DefaultRouter()
router.register(r'posts', PostsViews)


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'users', UserViews.as_view()),
    url(r'^hello_world', hello_world)
]