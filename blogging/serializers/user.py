# -*- coding: utf-8 -*-

from django import db
from django.contrib.auth.models import User
from django.views.defaults import bad_request

from rest_framework import serializers

from blogging.models.User import UserModel


class UserSerializer(serializers.HyperlinkedModelSerializer):
    username = serializers.CharField(source='user_auth.username')
    password = serializers.CharField(source='user_auth.password', write_only=True)
    email = serializers.CharField(source='user_auth.email')

    def create(self, validated_data):
        """
        Overide create method create on serializer to create an django user and added to UserModel with our fields
        :param validated_data: (dict) contains auth_user with username, password and email, and firstname and lastname
        :return: UserModel created
        """
        user_auth = validated_data['user_auth']
        user = User.objects.create(username=user_auth.get('username'), email=user_auth.get('email'))
        user.set_password(user_auth.get('password'))
        user.save()
        return UserModel.objects.create(**{
            'user_auth': user,
            'firstname': validated_data.get('firstname'),
            'lastname': validated_data.get('lastname'),
        })

    class Meta:
        model = UserModel
        fields = ('id', 'username', 'firstname', 'lastname', 'password', 'email')
        write_only_fields = ('password',)
        read_only_fields = ('is_staff', 'is_superuser', 'is_active', 'date_joined',)
