# -*- coding: utf-8 -*-

from rest_framework.test import APITransactionTestCase

from blogging.serializers.post import PostSerializer
from blogging.models.posts import PostsModel
from blogging.models.User import UserModel, User


class PostsSerializerTest(APITransactionTestCase):
    username = 'cordelia'
    password = 'Cordelia1'
    email = 'cordelia@cordelia.com'
    firstname = 'Cordelia'
    lastname = 'Burton'
    post = {
        'title': 'test-title',
        'body': 'this is the body of the post',
        'img': 'https://pbs.twimg.com/profile_images/744884875975548928/Dh5MccXJ.jpg',
    }
    bad_post = {
        'title': 'test-title',
        'img': 'https://pbs.twimg.com/profile_images/744884875975548928/Dh5MccXJ.jpg',
    }

    def setUp(self):
        # We create an auth user
        user = User.objects.create(username=self.username, email=self.email)
        user.set_password(self.password)
        user.save()

        # create user_model that will be the post owner
        owner = UserModel.objects.create(user_auth=user, firstname=self.firstname, lastname=self.lastname)

        # create post_model to perform test
        PostsModel.objects.create(**self.post, owner=owner)

    def test_getting_data(self):
        query_result = PostsModel.objects.all()
        serializer_result = PostSerializer(query_result, many=True)
        post = serializer_result.data[0]
        expected_result_data = [{
            'title': self.post['title']
        }]
        # check we have 1 posts
        self.assertEqual(len(serializer_result.data), 1)

        # check post containst same title as we create
        self.assertEqual(post['title'], self.post['title'])

        # check post containst same body as we create
        self.assertEqual(post['body'], self.post['body'])

        # check post containst same img as we create
        self.assertEqual(post['img'], self.post['img'])

        # check post containst same img as we create
        self.assertFalse(post['deleted'])

    def test_creating_data(self):
        serializer = PostSerializer(data=self.post)

        # Check data is valid
        self.assertTrue(serializer.is_valid())

        owner = UserModel.objects.get(firstname=self.firstname)
        serializer.save(owner=owner)

        models = PostsModel.objects.all()

        # Check post is created
        self.assertEqual(len(models), 2)

    def test_serializer_not_valid_create(self):
        serializer = PostSerializer(data=self.bad_post)

        # Check data is not valid
        self.assertFalse(serializer.is_valid())
