Project goal is learn how to django rest framework do an api rest

This is the same project as [kasappeal](https://bitbucket.org/bergran/kasappeal)
but is done in django rest framework

# Requirements

* Ubuntu 17.04
* Python 3.5.4
* docker 17.05.0-ce

# Dev dependencies

You can install dependencies with `pip install -r requirementes.txt`
or simple just run with docker, this dude know what he is doing (H)

# Docker

## install

* Clone project in your machine
* Execute `sh build.sh`. This command will build app image

## Deploy

After build follow next steps:

* Execute `sh run.sh`.

### Authentication
Authentication is going to be with tokens, the system implemented is JWT.
You will have to authenticate with your user on `/api-auth`. This Request
will return a token.
Token is going to be available 5 minutes and it can not refresh token.
header authentication is `Authorization` and his contain is `JWT + token`

### Request

#### HELLO WORLD

* hello world (method GET).
  * Authorization Header: no
  * URI: /api/v1/hello_world

#### AUTHENTICATE

* auth (method POST).
  * Authorization Header: no
  * URI: /api-auth/
  * params:
    * username:
      * attribute: str
    * password:
      * attribute: str

#### USER

* Create User (method POST).
  * Authorization Header: yes
  * URI: /api/v1/users
  * params:
    * firstname:
      * attribute: str
    * lastname:
      * attribute: str
    * username:
      * attribute: str
    * password:
      * attribute: str
    * email:
      * attribute: str

#### POSTS
* Get posts (method GET).
  * Authorization Header: yes
  * URI: /api/v1/posts/?page={page_number}
  * query_string:
    * order_by:
      * attribute: str
      * choices: ['date_create', 'title']
      * default: date_create
      * optional
    * order_type:
      * attribute: str
      * choices: ['DESC', 'ASC']
      * default: DESC
      * optional
    * date_start(timestamp):
      * attribute: float
      * default: 0
    * date_end(timestamp):
      * attribute: float
      * optional
      * default: today, will call time when execute, only if default
    * user:
      * attribute: str
      * optional
      * it's the username, e.g 'cordelia'
  * params:
    * page:
      * attribute: int
* get Post (method GET).
  * Authorization Header: yes
  * URI: /api/v1/posts/{id}/
  * Params:
    * id:
      * attribute: str
* Create post (method POST).
  * Authorization Header: yes
  * URI: /api/v1/posts/
  * params:
    * title:
      * attribute: str
    * img:
      * attribute: str
    * body:
      * attribute: str
* Delete Post (method DELETE).
  * Authorization Header: yes
  * URI: /api/v1/posts/{id}/
  * Params:
    * id:
      * attribute: str
