#!/bin/python3

from json import loads

from django.contrib.auth.models import User

from rest_framework.test import APITestCase
from rest_framework import status


class TestAuthApiView(APITestCase):
    username = 'cordelia'
    password = 'Cordelia1'
    bad_user = 'cordeliA'
    bad_password = 'Cordelia'

    def setUp(self):
        user = User.objects.create(username=self.username)
        user.set_password(self.password)
        user.save()

    def test_api_auth_error_no_(self):
        request = self.client.post('/api-auth/')
        self.assertEqual(request.status_code, status.HTTP_400_BAD_REQUEST)

    def test_api_auth_get_token(self):
        request = self.client.post('/api-auth/', {
        'username': self.username,
        'password': self.password
        })
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        response = loads(request.content)
        self.assertTrue('token' in response.keys())

    def test_api_auth_bad_authentication_bad_user(self):
        request = self.client.post('/api-auth/', {
        'username': self.bad_user,
        'password': self.password
        })
        self.assertEqual(request.status_code, status.HTTP_400_BAD_REQUEST)

    def test_api_auth_bad_authentication_bad_password(self):
        request = self.client.post('/api-auth/', {
        'username': self.username,
        'password': self.bad_password
        })
        self.assertEqual(request.status_code, status.HTTP_400_BAD_REQUEST)
