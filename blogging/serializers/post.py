# -*- coding: utf-8 -*-
from django.utils.timezone import localtime, datetime

from rest_framework import serializers

from blogging.models.posts import PostsModel


class PostSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.firstname')

    class Meta:
        model = PostsModel
        fields = ('id', 'title', 'body', 'img', 'date_create', 'date_modify', 'owner', 'deleted')
        read_only_fields = ('id', 'date_create', 'date_modify', 'deleted')

def PostsFilterSerializer():
    now = datetime.now().timestamp()
    # patch to fix max value on date_end, now always be now when executed
    class Wrapper(serializers.Serializer):
        date_start = serializers.FloatField(required=False,
                                            default=localtime().replace(
                                                hour=0,
                                                minute=0,
                                                second=0,
                                                microsecond=0
                                            ).timestamp(),
                                            min_value=datetime(1970, 1, 2).timestamp())
        date_end = serializers.FloatField(required=False,
                                          default=now,
                                          max_value=now)
        order_by = serializers.ChoiceField(choices=('date_create', 'title'), default='date_create', required=False)
        order_type = serializers.ChoiceField(choices=('ASC', 'DESC'), default='DESC', required=False)

        def create(self, validated_data):
            pass

        def update(self, instance, validated_data):
            pass
    return (Wrapper)
