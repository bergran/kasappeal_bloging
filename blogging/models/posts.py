# -*- coding: utf-8 -*-

from django.db import models

from blogging.models.User import UserModel


class PostsModel(models.Model):
    title = models.CharField(max_length=50)
    body = models.CharField(max_length=200)
    img = models.CharField(max_length=400)
    date_create = models.DateTimeField(auto_now_add=True)
    date_modify = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)
    owner = models.ForeignKey(UserModel, on_delete=models.CASCADE)

    class Meta:
        db_table = 'Posts'
        app_label = 'blogging'
