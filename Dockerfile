FROM python:3.5.4-alpine

COPY . /usr/src/app
WORKDIR /usr/src/app

RUN pip install -r requirements.txt

EXPOSE 8000
VOLUME /usr/src/app

RUN python manage.py makemigrations blogging && \
    python manage.py migrate

ENTRYPOINT ["gunicorn", "kasappeal.wsgi", "-c", "gunicorn_config.py"]
