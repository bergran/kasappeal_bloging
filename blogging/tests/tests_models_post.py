# -*- coding: utf-8 -*-

from rest_framework.test import APITransactionTestCase

from blogging.models.posts import PostsModel
from blogging.models.User import UserModel, User


class PostModelTests(APITransactionTestCase):
    owner = {
        'username': 'cordelia',
        'password': 'Cordelia1',
        'firstname': 'Cordelia',
        'lastname': 'Burton',
        'email': 'cordelia@cordelia.com'
    }
    post = {
        'title': 'test-title',
        'body': 'test-body',
        'img': 'https://pbs.twimg.com/profile_images/744884875975548928/Dh5MccXJ.jpg'
    }
    new_post = {
        'title': 'test-title1',
        'body': 'test-body1',
        'img': 'https://pbs.twimg.com/profile_images/744884875975548928/Dh5MccXJ.jpg'
    }

    def setUp(self):
        # Creating user
        user = User.objects.create(
                                   username=self.owner['username'],
                                   email=self.owner['email'])
        user.set_password(self.owner['password'])

        # Creating owner to Post
        owner = UserModel.objects.create(
                                         user_auth=user,
                                         firstname=self.owner['firstname'],
                                         lastname=self.owner['lastname']
                                        )
        # Creating post
        PostsModel.objects.create(**self.post, owner=owner)

    def test_get_all(self):
        posts = PostsModel.objects.all()

        # Check return 1 post
        self.assertEqual(len(posts), 1)

        # Check post it's the same
        self.assertEqual(posts[0].title, self.post['title'])
        self.assertEqual(posts[0].body, self.post['body'])
        self.assertEqual(posts[0].img, self.post['img'])

    def test_create_post(self):
        owner = UserModel.objects.get(firstname=self.owner['firstname'])
        post = PostsModel.objects.create(**self.new_post, owner=owner)

        posts = PostsModel.objects.all()

        # Check now return 2 posts on get all posts
        self.assertEqual(len(posts), 2)

        # Check post it's the same
        self.assertEqual(posts[1].title, self.new_post['title'])
        self.assertEqual(posts[1].body, self.new_post['body'])
        self.assertEqual(posts[1].img, self.new_post['img'])
        self.assertFalse(posts[1].deleted)
