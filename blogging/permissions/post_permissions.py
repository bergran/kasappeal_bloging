# -*- coding: utf-8 -*-

from rest_framework import permissions

from blogging.models.User import UserModel


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object
    to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Only on safe methods will have permissions to get object
        if request.method in permissions.SAFE_METHODS:
            return True

        # if owner obj is same as user auth then will have permission to write
        return obj.owner == UserModel.objects.get(user_auth=request.user)
