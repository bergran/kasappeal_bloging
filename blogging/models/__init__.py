from .posts import PostsModel
from .User import UserModel

__all__ = ['PostsModel', 'UserModel']
